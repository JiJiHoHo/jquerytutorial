
//we create functions alongside with their related array keys in studentArray variable//

var studentArray={
	"John":function(){
		return "Hey im John"
	},
	'Maria':function(){
		return "Hey im Maria"
	}
}
var studentNameOne="John"
// check studentNameOne variable value is one of the array keys of studentArray variable with tunary operator//
// if the studentNameOne variable value is one of the array keys of studentArray variable 
// 	the function which is related to the array key will be implemented
// thats why return is the last statement in this code
//you can test by changing the studentNameOne variable value

//we can also create that function with switch case too.
// but to create more maintainable code we have to assign array keys with their function//
console.log(studentNameOne in studentArray ? studentArray[studentNameOne]() :"the key is not related to studentArray's keys")